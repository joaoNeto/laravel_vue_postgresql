<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|integer',
            'birthdate' => 'required|date'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'the name is required',
            'name.string' => 'the name must be a text',

            'email.required' => 'the email is required',
            'email.email' => 'the email must be a valid email',

            'phone.required' => 'the phone is required',
            'phone.integer' => 'the phone must have just numbers',

            'birthdate.required' => 'the birthdate is required',
            'birthdate.string' => 'O birthdate precisa ser uma string',
        ];
    }

}
