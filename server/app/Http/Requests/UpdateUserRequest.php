<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string',
            'email' => 'nullable|email',
            'phone' => 'nullable|integer',
            'birthdate' => 'nullable|date'
        ];
    }

    public function messages()
    {
        return [
            'name.string' => 'the name must be a text',
            'email.email' => 'the email must be a valid email',
            'phone.integer' => 'the phone must have just numbers',
            'birthdate.string' => 'O birthdate precisa ser uma string',
        ];
    }

}
