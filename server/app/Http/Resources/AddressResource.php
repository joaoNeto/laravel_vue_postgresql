<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "postcode" => $this->postcode,
            "country" => $this->country,
            "state" => $this->state,
            "city" => $this->city,
            "street_name" => $this->street_name,
            "street_number" => $this->street_number,
            "created_at" => $this->created_at->format("Y-m-d"),
            "updated_at" => $this->updated_at->format("Y-m-d")         
        ];
    }
}
