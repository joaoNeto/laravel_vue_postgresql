<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory(25)->create();

        $users->each(function ($user) {
            $numbersOfAddresses = rand(1,5);
    
            Address::factory($numbersOfAddresses)->create([ 'user_id' => $user->id ]);    
        });
    }
}
