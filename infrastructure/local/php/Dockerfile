FROM php:7.4-fpm

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get upgrade -y -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -qq -y \
      build-essential \
      curl \
      git \
      vim \
      zip unzip \
      redis-server \
      libhiredis-dev \
      autoconf \
      nginx-extras \
      supervisor \
    && install-php-extensions \
      uuid \
      gmp \
      amqp \
      yaml \
      bz2 \
      gd \
      pgsql \
      pdo_pgsql \
      redis \
      xsl \
      zip \
      sockets

RUN rm -Rf /var/cache/apk/*

RUN yes | docker-php-ext-enable sockets \
    && docker-php-source delete \
    && rm -rf /tmp/*

RUN pecl channel-update pecl.php.net \
	&& pecl install

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && ln -s $(composer config --global home) /root/composer
ENV PATH=$PATH:/root/composer/vendor/bin COMPOSER_ALLOW_SUPERUSER=1

RUN composer global require hirak/prestissimo --ignore-platform-reqs

WORKDIR /var/www

RUN chmod -R 755 /var/www

COPY ./infrastructure/local/php/local.ini /usr/local/etc/php/conf.d/local.ini

EXPOSE 9000