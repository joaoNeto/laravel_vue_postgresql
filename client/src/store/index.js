/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
    listUserRequestInProccess: false,
  },
  mutations: {
    setUsers(state, value) {
      state.users = value;
    },
    setListUserRequestInProccess(state, value) {
      state.listUserRequestInProccess = value;
    },
  },
  actions: {
    async listUsers({ commit }, currentPage = 1) {
      commit('setListUserRequestInProccess', true);
      const response = await Vue.prototype.$http.get('/user', { params: { page: currentPage } });
      commit('setUsers', response.data.data);
      commit('setListUserRequestInProccess', false);
    },
    insertUser({}, payload) {
      return Vue.prototype.$http.post('/user', payload);
    },
  },
});
